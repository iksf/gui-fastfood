extern crate gtk;
extern crate gdk;
use std::rc::Rc;
use std::cell::{Cell, RefCell};
use std::ops::Deref;
use gtk::prelude::*;
use std::path::Path;
use std::fmt::write;
//use gdk::{WindowExt};
use gtk::{GridExt, Grid, Label, LabelExt};
#[derive(Debug)]


pub struct Choice {
    grid: gtk::Grid,
    title: gtk::Label,
    description: gtk::Label,
    metadata: Vec<String>,
    count: usize,
    image: gtk::Image,
}

impl Deref for Choice {
    type Target = gtk::Grid;
    fn deref(&self) -> &Self::Target {
        &self.grid
    }
}

impl Choice {
    pub fn new(label: &str, desc: &str, image: &str) -> Self {
        let dir: &'static str = env!("CARGO_MANIFEST_DIR");
        let image = {
            let mut path = String::new();
            path.push_str(dir);
            path.push_str("/assets/");
            path.push_str(image);
            println!("{}", path);
            gtk::Image::new_from_file(Path::new(&path))
        };
        let grid = gtk::Grid::new();
        grid.insert_row(2);
        grid.insert_column(2);
        let title = gtk::Label::new(label);
        let description = gtk::Label::new(desc);
        grid.attach(&title, 0, 0, 1, 1);
        grid.attach(&description, 1, 0, 1, 2);
        grid.attach(&image, 0, 1, 1, 1);
        grid.show_all();
        Self {
            grid,
            title,
            image,
            description,
            count: 0,
            metadata: Vec::new(),
        }
    }
    pub fn check_self(&self, search: &str) -> bool {
        for i in self.metadata.iter() {
            if i.len() < search.len() {
                continue;
            } else {
                if search == &i[0..search.len()] {
                    return true;
                }
            }
        }
        false
    }
    pub fn with_meta(mut self, string: &str) -> Self {
        self.metadata.push(string.to_lowercase());
        self
    }
}
#[derive(Debug)]
pub struct ChoiceGrid {
    grid: gtk::Grid,
    choices: Vec<Choice>,
    menu: Order,
}
impl ChoiceGrid {
    pub fn new() -> Self {
        let choice_grid = gtk::Grid::new();
        choice_grid.insert_row(5);
        choice_grid.insert_column(3);
        choice_grid.set_row_homogeneous(true);
        choice_grid.set_column_homogeneous(true);
        let choices = Vec::new();
        let mut myself = Self {
            grid: choice_grid,
            choices,
            menu: Order::new(),
        };
        myself.grid.attach(&*myself.menu, 2, 0, 1, 5);
        myself.update_view(None);
        myself
    }
    pub fn add(&mut self, choice: Choice) {
        self.choices.push(choice);
        self.update_view(None);
    }
    pub fn update_view(&mut self, search: Option<&str>) {
        let currently_viewable: Vec<&Choice> = {
            match search {
                Some(search_term) => {
                    self.choices
                        .iter()
                        .filter(|x| x.check_self(&search_term.to_lowercase()))
                        .collect()
                }
                None => self.choices.iter().collect(),
            }
        };
        let current = self.grid.get_children();
        for i in current.iter() {
            self.grid.remove(i)
        }
        let mut x = 0;
        let mut y = 0;
        for (index, item) in currently_viewable.iter().enumerate() {
            self.grid.attach(&***item, x, y, 1, 1);
            if index == 2 * y as usize + 1 {
                y += 1;
                x = 0;
            } else {
                x += 1;
            }
        }

    }
}
impl Deref for ChoiceGrid {
    type Target = gtk::Grid;
    fn deref(&self) -> &Self::Target {
        &self.grid
    }
}
#[derive(Debug)]
pub struct Order {
    grid: gtk::Grid,
    labels: Vec<gtk::Label>,
}
impl Order {
    pub fn new() -> Self {
        Self {
            labels: Vec::new(),
            grid: gtk::Grid::new(),
        }
    }
    pub fn add(&mut self, input: &str) {
        let label = gtk::Label::new(input);
        self.labels.push(label);
        self.update();
    }
    pub fn remove(&mut self, index: usize) {
        self.labels.remove(index);
        self.update();
    }
    pub fn update(&mut self) {
        for i in self.grid.get_children() {
            self.grid.remove(&i)
        }
        for (index, i) in self.labels.iter().enumerate() {
            self.grid.attach(i, 0, index as i32, 1, 1);
        }
    }
}
impl Deref for Order {
    type Target = gtk::Grid;
    fn deref(&self) -> &Self::Target {
        &self.grid
    }
}
fn main() {
    gtk::init().unwrap();
    let window = gtk::Window::new(gtk::WindowType::Toplevel);
    window.set_property_default_height(800);
    window.set_property_default_width(800);
    let grid = gtk::Grid::new();
    let current_scroll_level = gtk::Adjustment::new(0., 0., 1., 0.1, 0.1, 0.);
    let scrolled_window = gtk::ScrolledWindow::new(None, Some(&current_scroll_level));
    let mut choice_grid = ChoiceGrid::new();
    choice_grid.add(
        Choice::new("Coke", "Coca-cola", "coke.png")
            .with_meta("Drink")
            .with_meta("Soft")
            .with_meta("Coke")
            .with_meta("Soda")
            .with_meta("Coca-cola"),
    );
    choice_grid.add(
        Choice::new("Diet Coke", "Coca-cola diet", "diet_coke.png")
            .with_meta("Diet")
            .with_meta("Drink")
            .with_meta("Soft")
            .with_meta("Coke")
            .with_meta("Soda")
            .with_meta("Coca-cola"),
    );
    choice_grid.add(
        Choice::new("Burger", "Beef burger", "burger.jpeg")
            .with_meta("Food")
            .with_meta("Meat")
            .with_meta("Burger")
            .with_meta("Beef")
            .with_meta("Meat"),
    );
    choice_grid.add(
        Choice::new("Chips", "Delicious fries", "chips.png")
            .with_meta("Food")
            .with_meta("Chips")
            .with_meta("Fries")
            .with_meta("Vegetatrian")
            .with_meta("Sides"),
    );
    scrolled_window.add_with_viewport(&choice_grid.clone());
    let choice_grid_rc = Rc::new(RefCell::new(choice_grid));
    scrolled_window.set_hexpand(true);
    scrolled_window.set_vexpand(true);
    let header_bar = gtk::Grid::new();
    let search = gtk::Entry::new();
    search.connect_property_text_notify({
        let search_box = search.clone();
        let choice_grid = choice_grid_rc.clone();
        move |_| {
            let mut ptr = choice_grid.borrow_mut();
            ptr.update_view(Some(&search_box.get_buffer().get_text()));
        }
    });
    header_bar.attach(&search, 0, 0, 1, 1);
    header_bar.attach(&gtk::Label::new("Fast food Co"), 1, 0, 1, 1);
    grid.insert_row(3);
    grid.insert_column(3);
    grid.attach(&header_bar, 0, 0, 1, 1);
    grid.attach(&scrolled_window, 0, 1, 2, 3);
    let order = Order::new();
    grid.attach(&*order, 3, 1, 1, 3);
    window.add(&grid);
    window.show_all();
    gtk::main();
}
